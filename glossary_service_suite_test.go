package glossary_service_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestGlossaryService(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GlossaryService Suite")
}
