package glossary_service_test

import (
  "bytes"
  "glossary-service/pkg/tag"
  "glossary-service/pkg/server"
  "fmt"
  "github.com/gin-gonic/gin"
  "github.com/google/uuid"
  . "github.com/onsi/ginkgo"
  . "github.com/onsi/gomega"
  "net/http"
  "net/http/httptest"
)

var _ = Describe("tags", func() {

  router := tag.WithRoutes(server.ServerFactory())
  tagsClient := TagsClient{router}

  Describe("Adding tags", func() {
    Context("Brand New Tag", func() {
      It("Should be created", func() {
        name := fmt.Sprintf("Tag - %v", uuid.New())
        code, category := tagsClient.add(tag.Tag{Name: name})

        Expect(code).To(Equal(201))
        Expect(category.Id).To(Not(Equal(uuid.Nil)))
        Expect(category.Name).To(Equal(name))
      })
    })
    Context("Existing Tag", func() {
      It("Should result in an error", func() {

      })
    })
  })

  Describe("Searching tags", func() {
    Context("Existing Tag", func() {
      name := fmt.Sprintf("Tag - %v", uuid.New())
      _, category := tagsClient.add(tag.Tag{Name: name})
      code, resolvedCategory := tagsClient.findByName(name)
      Expect(code).To(Equal(200))
      Expect(resolvedCategory.Name).To(Equal(category.Name))
      Expect(resolvedCategory.Id).To(Equal(category.Id))

    })
  })

  Describe("Downloading tags", func() {
    Context("No tags", func() {
      It("Should return an empty list", func() {
        if err := tag.Truncate(); err != nil {
          panic(err)
        }
        w := tagsClient.getAll()

        // move to client
        Expect(w.Code).To(Equal(200))
        Expect(w.Body.String()).To(Equal(`{"tags":[]}`))
      })
    })
  })

})

type TagsClient struct {
  Router *gin.Engine
}

// TODO encapsulate the api contract
func (c TagsClient) getAll() *httptest.ResponseRecorder {
  w := httptest.NewRecorder()
  req, _ := http.NewRequest("GET", "/tags", nil)
  c.Router.ServeHTTP(w, req)
  return w
}

func (c TagsClient) add(category tag.Tag) (int, *tag.Tag) {
  w := httptest.NewRecorder()

  req, _ := http.NewRequest("POST", "/tags", bytes.NewReader(category.AsJson()))
  c.Router.ServeHTTP(w, req)
  data := tag.JsonToTag(w.Body.Bytes())
  return w.Code, data
}

func (c TagsClient) findByName(name string) (int, *tag.Tag) {
  w := httptest.NewRecorder()

  req, _ := http.NewRequest("GET", fmt.Sprintf("/tag?q=%v", name), bytes.NewReader(make([]byte, 0)))
  c.Router.ServeHTTP(w, req)
  data := tag.JsonToTag(w.Body.Bytes())
  return w.Code, data

}
