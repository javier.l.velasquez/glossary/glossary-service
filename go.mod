module glossary-service

go 1.14

require (
	github.com/georgysavva/scany v0.2.8
	github.com/gin-gonic/gin v1.7.1
	github.com/go-playground/validator/v10 v10.5.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.2.0
	github.com/jackc/pgx/v4 v4.11.0
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/onsi/ginkgo v1.16.1
	github.com/onsi/gomega v1.11.0
	github.com/ugorji/go v1.2.5 // indirect
	golang.org/x/sys v0.0.0-20210414055047-fe65e336abe0 // indirect
	golang.org/x/text v0.3.6 // indirect
)
