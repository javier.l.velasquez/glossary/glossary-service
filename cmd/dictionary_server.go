package main

import (
	"glossary-service/pkg/tag"
  "glossary-service/pkg/glossary"
  "glossary-service/pkg/server"
)



func main() {
  server := server.ServerFactory()
  tag.WithRoutes(server)
  glossary.WithRoutes(server)
  server.Run(":8080") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")

}
