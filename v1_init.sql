CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- drop table glossory_tags;
-- drop table glossary;
-- drop table tags;
create table if not exists tags
(
    id       uuid primary key     default uuid_generate_v4(),
    name     varchar(50) not null unique,
    created  timestamp   not null default now(),
    modified timestamp
);

create table if not exists glossary
(
    id         uuid primary key     default uuid_generate_v4(),
    name       varchar(50) not null unique,
    definition text        not null unique,
    created    timestamp   not null default now(),
    modified   timestamp
);

create table if not exists glossary_tags
(
    id      uuid primary key default uuid_generate_v4(),
    word_id uuid not null,
    tag_id  uuid not null,
    FOREIGN KEY (word_id) REFERENCES glossary (id),
    FOREIGN KEY (tag_id) REFERENCES tags (id),
    UNIQUE (word_id, tag_id)
);

CREATE OR REPLACE FUNCTION trigger_set_timestamp()
    RETURNS TRIGGER AS
$$
BEGIN
    NEW.modified = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_glossary_modified
    BEFORE UPDATE
    ON glossary
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

CREATE TRIGGER set_tag_modified
    BEFORE UPDATE
    ON glossary
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

ALTER TABLE glossary
    ADD searchable TSVECTOR not null;
CREATE INDEX IF NOT EXISTS idx_glossary_search ON glossary USING gin (searchable);

ALTER TABLE tags
    ADD searchable TSVECTOR not null;
CREATE INDEX IF NOT EXISTS idx_tag_search ON tags USING gin (searchable);
