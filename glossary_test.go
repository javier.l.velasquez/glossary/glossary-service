package glossary_service_test

import (
  "bytes"
  "glossary-service/pkg/glossary"
  "glossary-service/pkg/server"
  "glossary-service/pkg/tag"
  "fmt"
  "github.com/gin-gonic/gin"
  "github.com/google/uuid"
  . "github.com/onsi/ginkgo"
  . "github.com/onsi/gomega"
  "net/http"
  "net/http/httptest"
  "sync"
)

var _ = Describe("Glossary", func() {

  router := tag.WithRoutes(glossary.WithRoutes(server.ServerFactory()))
  dictionaryClient := GlossaryClient{router}
  tagClient := TagClient{router}
  testGlossaryWorkflows := TestGlossaryWorkflows{dictionaryClient}

  Describe("Adding to the Glossary", func() {
    Context("Brand New Word", func() {
      It("Should be created", func() {

        _, newTag := tagClient.add(tag.Tag{Name: fmt.Sprintf("tag: %v", uuid.New())})

        Expect(testGlossaryWorkflows.CreateWord(*newTag)).To(Not(BeNil()))
      })
      It("Should be created with tags", func() {

        Expect(testGlossaryWorkflows.CreateWord()).To(Not(BeNil()))
      })
    })
    Context("Existing Word", func() {
      It("Should result in an error", func() {
      })
    })
  })

  Describe("Browsing the glossary", func() {
    Context("Populated Glossary", func() {
      It("Should Return Entries", func() {
          if err := glossary.Truncate(); err != nil {
            Fail(err.Error())
          } else {
            var wg sync.WaitGroup
            wg.Add(1)
            // We use a batch insert API for as an example should normally go through external inteface (e.g. REST)
            for i := 0; i < 1; i++ {
              go func() {
                var words []glossary.Word
                for j := 0; j < 1; j++ {
                  name := fmt.Sprintf("New Word: %v", uuid.New())

                  words = append(words, glossary.Word{
                    Id:         uuid.Nil,
                    Name:       name,
                    Definition: uuid.NewString(),
                  })
                }
                glossary.BatchInsert(words)
                wg.Done()
              }()
            }
            wg.Wait()
            word := testGlossaryWorkflows.CreateWord()
            code, words := dictionaryClient.Search(word.Name)
            Expect(code).To(Equal(http.StatusOK))
            Expect(words.Size()).To(Equal(1))

          }
      })
    })
  })
})

type TestGlossaryWorkflows struct {
  D GlossaryClient
}

func (t TestGlossaryWorkflows) CreateWord(tags ...tag.Tag) *glossary.Word  {
  name := fmt.Sprintf("New Word: %v", uuid.New())

  createdCode, word := t.D.add(glossary.Word{
    Id:         uuid.Nil,
    Name:       name,
    Definition: uuid.NewString(),
    Tags:       tags,
  })

  Expect(createdCode).To(Equal(http.StatusCreated))
  Expect(word.Id).To(Not(Equal(uuid.Nil)))
  Expect(word.Name).To(Equal(name))

  code, resolvedWord := t.D.FindById(word.Id)
  Expect(code).To(Equal(http.StatusOK))
  Expect(resolvedWord.Id).To(Equal(word.Id))
  Expect(resolvedWord.Name).To(Equal(name))

  for _, tag := range tags {
    wordFound := false
    for _, persistedTag := range resolvedWord.Tags {
      if tag.Id == persistedTag.Id {
        Expect(tag.Name).To(Equal(persistedTag.Name))
        wordFound = true
      }
    }
    if !wordFound {
      Fail(fmt.Sprintf("unable to find persisted tag: %v", tag.Name))
    }
  }

  return word

}

type GlossaryClient struct {
  Router *gin.Engine
}

type TagClient struct {
  Router *gin.Engine
}

func (t TagClient) add(tag tag.Tag) (int, *tag.Tag) {
  w := httptest.NewRecorder()

  req, _ := http.NewRequest("POST", "/tags", bytes.NewReader(tag.AsJson()))
  t.Router.ServeHTTP(w, req)
  data := tag.JsonToTag(w.Body.Bytes())
  return w.Code, data
}

// TODO encapsulate the api contract
func (c GlossaryClient) getAll() *httptest.ResponseRecorder {
  w := httptest.NewRecorder()
  req, _ := http.NewRequest("GET", "/glossary", nil)
  c.Router.ServeHTTP(w, req)
  return w
}

func (c GlossaryClient) add(word glossary.Word) (int, *glossary.Word) {
  w := httptest.NewRecorder()

  req, _ := http.NewRequest("POST", "/glossary", bytes.NewReader(word.AsJson()))
  c.Router.ServeHTTP(w, req)
  data := glossary.JsonToWord(w.Body.Bytes())
  return w.Code, data
}

func (c GlossaryClient) FindById(id uuid.UUID) (int, *glossary.Word) {
  w := httptest.NewRecorder()

  req, _ := http.NewRequest("GET", fmt.Sprintf("/glossary/%v", id), bytes.NewReader(make([]byte, 0)))
  c.Router.ServeHTTP(w, req)
  data := glossary.JsonToWord(w.Body.Bytes())
  return w.Code, data
}

func (c GlossaryClient) Search(name string) (int, *glossary.Words) {
  w := httptest.NewRecorder()

  req, _ := http.NewRequest("GET", fmt.Sprintf("/glossary?q=%v", name), bytes.NewReader(make([]byte, 0)))
  c.Router.ServeHTTP(w, req)
  data := glossary.JsonToWords(w.Body.Bytes())
  return w.Code, data

}

