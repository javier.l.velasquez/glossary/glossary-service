package database

import (
  "context"
  "fmt"
  "github.com/jackc/pgx/v4"
  "github.com/jackc/pgx/v4/pgxpool"
  _ "github.com/jackc/pgx/v4/pgxpool"

  "os"
)

var pool *pgxpool.Pool

func init() {
  pool = GeneratePool()
}
func GeneratePool() *pgxpool.Pool {
  background := context.Background()
  conn, err := pgxpool.Connect(background, "postgresql://postgres:password@localhost:5432/glossary")
  if err != nil {
    fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
    os.Exit(1)
  }
  return conn
}


type SqlCommand struct {
  Sql  string
  Args []interface{}
}

// This code most likely has a resource leak and should not be used outside of testing
func BatchInsert(size int, commands []SqlCommand) error {
  batch := &pgx.Batch{}

  var currentBatch int
  for _, command := range commands {
    if currentBatch == size {
      br := pool.SendBatch(context.Background(), batch)
      br.Close()
      batch = &pgx.Batch{}
    } else {
      batch.Queue(command.Sql, command.Args...)
      currentBatch = currentBatch + 1
    }
  }
  br := pool.SendBatch(context.Background(), batch)
  br.Close()
  batch = &pgx.Batch{}

  return nil
}

func ExecuteTransactionAndIgnore(sql string, args ...interface{}) error {
  background := context.Background()
  //defer conn.Close()

  tx, err := pool.BeginTx(background, pgx.TxOptions{})

  defer tx.Rollback(background)

  _, err = tx.Exec(background, sql, args...)
  if err != nil {
    return err
  }

  return tx.Commit(background)
}


func ExecuteSqlInTransaction(result func(pgx.Row) error, sql string, args ...interface{}) error {
  background := context.Background()
  //defer conn.Close()

  tx, err := pool.BeginTx(background, pgx.TxOptions{})

  defer tx.Rollback(background)

  row := tx.QueryRow(background, sql, args...)
  err = result(row)

  if err != nil {
    return err
  }

  return tx.Commit(background)
}


func ExecuteTransaction(result func(tx pgx.Tx) error) error {
  background := context.Background()
  //defer conn.Close()

  tx, err := pool.BeginTx(background, pgx.TxOptions{})

  defer tx.Rollback(background)

  err = result(tx)

  if err != nil {
    return err
  }

  return tx.Commit(background)
}


func ExecuteTransactionAnd(background *context.Context, result func(pgx.Row) error, sql string, args ...interface{}) (pgx.Tx, error) {
  //defer conn.Close()

  tx, err := pool.BeginTx(*background, pgx.TxOptions{})

  row := tx.QueryRow(*background, sql, args...)
  err = result(row)

  if err != nil {
    return nil, err
  }

  return tx, nil
}


func WithinTransaction(tx pgx.Tx, sql string, args ...interface{}) (pgx.Tx, error) {
  background := context.Background()

  if _, err := tx.Exec(background, sql, args...); err == nil {
    return tx, nil
  } else {
    return nil, err
  }
}

func FindFirst(result func(pgx.Row) error, sql string, args ...interface{}) error {
  rows := pool.QueryRow(context.Background(), sql, args...)
  return result(rows)
}

func FindMultiple(result func(pgx.Row) error, sql string, args ...interface{}) error {
  if rows, err := pool.Query(context.Background(), sql, args...); err == nil {
    for rows.Next() {
      if err = result(rows); err != nil {
        return err
      }
    }
    return nil
  } else {
    return err
  }

}
