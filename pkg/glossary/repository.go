package glossary

import (
  "context"
  "encoding/json"
  "github.com/google/uuid"
  "github.com/jackc/pgx/v4"
  _ "github.com/jackc/pgx/v4/pgxpool"
  "glossary-service/pkg/database"
  "glossary-service/pkg/server"
  "glossary-service/pkg/tag"
  "log"
)

type Word struct {
  Id         uuid.UUID `json:"id" default:"00000000-0000-0000-0000-000000000000"`
  Name       string    `json:"name"`
  Definition string    `json:"definition"`
  Tags       []tag.Tag `json:"tags" default: []`
}

type Words struct {
  Words []Word `json:"words"`
}

func (w Words) Size() int {
  return len(w.Words)
}

func JsonToWord(b []byte) *Word {
  data := Word{}
  err := json.Unmarshal(b, &data)
  if err != nil {
    // TODO update log to include body as json
    log.Print(err)
    return &Word{}
  } else {
    return &data
  }
}

func JsonToWords(b []byte) *Words {
  data := Words{}
  err := json.Unmarshal(b, &data)
  if err != nil {
    // TODO update log to include body as json
    log.Print(err)
    return &Words{}
  } else {
    return &data
  }
}

func (c Word) AsJson() []byte {
  marshal, err := json.Marshal(c)
  if err != nil {
    panic(err)
  }
  return marshal
}

func BatchInsert(words []Word) error {
  var commands []database.SqlCommand
  for _, word := range words {
    commands = append(commands, database.SqlCommand{
      Sql:  "insert into glossary (name, definition, searchable) values ($1, $2, to_tsvector($3)) returning id",
      Args: []interface{}{word.Name, word.Definition, word.Name},
    })
  }
  return database.BatchInsert(10000, commands)
}

func Insert(word *Word) (*Word, error) {
  var id uuid.UUID
  background := context.Background()
  tx, err := database.ExecuteTransactionAnd(&background, func(row pgx.Row) error {
    return row.Scan(&id)
  }, "insert into glossary (name, definition, searchable) values ($1, $2, to_tsvector($3)) returning id", word.Name, word.Definition, word.Name)
  if err != nil {
    return nil, err
  }

  if len(word.Tags) > 0 {

    nestedTr, err := tx.Begin(background)
    if err != nil {
      return nil, err
    }

    for _, t := range word.Tags {
      _, err := database.WithinTransaction(nestedTr, "insert into glossary_tags (word_id, tag_id) values ($1, $2)", id, t.Id)
      if err != nil {
        return nil, err
      }
    }
    err = nestedTr.Commit(background)
    if err != nil {
      return nil, err
    }
  }

  err = tx.Commit(background)
  if err != nil {
    return nil, err
  }

  word.Id = id
  return word, nil
}

func Truncate() error {
  if err := database.ExecuteTransactionAndIgnore("truncate glossary_tags CASCADE;"); err != nil {
    return err
  }
  if err := database.ExecuteTransactionAndIgnore("truncate glossary CASCADE;"); err != nil {
    return err
  }
  if err := database.ExecuteTransactionAndIgnore("truncate tags CASCADE;"); err != nil {
    return err
  }
  return nil
}

func FindById(id uuid.UUID) (*Word, bool) {
  var words []Word

  sql := `select g.id, g.name, g.definition, t.id as tag_id, t.name as tag_name
          from glossary g
         left join glossary_tags gt on g.id = gt.word_id
         left join tags t on gt.tag_id = t.id where g.id = $1`
  // should handle error gracefully
 database.FindMultiple(func(row pgx.Row) error {
    var id string
    var name string
    var definition string
    var tagId string
    var tagName string

    if err := row.Scan(&id, &name, &definition, &tagId, &tagName); err == nil {
      words = append(words, Word{
        Id:         uuid.MustParse(id),
        Name:       name,
        Definition: definition,
        Tags:       []tag.Tag{{Id: uuid.MustParse(tagId), Name: tagName}},
      })
      return nil
    } else {
      // assuming error means empty tag handle this better
      words = append(words, Word{
        Id:         uuid.MustParse(id),
        Name:       name,
        Definition: definition,
        Tags:       []tag.Tag{},
      })
      return err
    }
  }, sql, id)

  if len(words) == 0 {
    return nil, false
  } else  if len(words) == 1 {
    return &words[0], true
  } else {
    word := words[0]
    for i, w := range words {
      if i > 0 {
        word.Tags = append(word.Tags, w.Tags...)
      }
    }
    return &word, true
  }

}

func List(query string, options *server.PaginationOptions) ([]Word, error) {
  var words []Word
  var sql string
  var args []interface{}
  if len(query) == 0 {
    sql = "select id, name, definition from glossary order by id limit $1 offset $2"
    args = []interface{}{options.Limit, options.Offset}
  } else {
    sql = "select id, name, definition from glossary WHERE searchable @@ plainto_tsquery($1) order by id limit $2 offset $3"
    args = []interface{}{query, options.Limit, options.Offset}

  }

  err := database.FindMultiple(func(row pgx.Row) error {
    var id string
    var name string
    var definition string
    if err := row.Scan(&id, &name, &definition); err == nil {
      words = append(words, Word{
        Id:         uuid.MustParse(id),
        Name:       name,
        Definition: definition,
      })
      return nil
    } else {
      return err
    }
  }, sql, args...)
  return words, err
}
