package glossary

import (
  "glossary-service/pkg/server"
  "fmt"
  "github.com/gin-gonic/gin"
  "github.com/google/uuid"
  "net/http"
  "strconv"
)

func extractPaginationOptions(c *gin.Context) (*server.PaginationOptions, error) {
  var limit int
  var offset int
  var err error
  if limit, err = strconv.Atoi(c.DefaultQuery("limit", "25")); err != nil {
    return nil, err
  }

  if offset, err = strconv.Atoi(c.DefaultQuery("offset", "0")); err != nil {
    return nil, err
  }
  return &server.PaginationOptions{Limit: limit, Offset: offset}, nil
}


func WithRoutes(r *gin.Engine) *gin.Engine {

  r.GET("/glossary", func(c *gin.Context) {
    server.ConfigureCORS(c)

    if paginationOptions, err := extractPaginationOptions(c); err == nil {
      if words, err := List(server.ExtractSearchQueryParameter(c), paginationOptions); err == nil {
        c.JSON(http.StatusOK, gin.H{"words": words})
      } else {
        c.JSON(http.StatusInternalServerError, gin.H{
          "message": err,
        })
      }
    } else {
      c.JSON(http.StatusBadRequest, gin.H{
        "message": err,
      })
    }

  })

  r.GET("/glossary/:id", func(c *gin.Context) {
    server.ConfigureCORS(c)

    if id, err := uuid.Parse(c.Param("id")); err == nil {
      if word, ok := FindById(id); ok {
        c.JSON(http.StatusOK, word)
      } else {
        c.JSON(http.StatusNotFound, gin.H{
          "message": fmt.Sprintf("unable to find word with the id: %v", id),
        })
      }
    } else {
      c.JSON(http.StatusBadRequest, gin.H{
        "message": err.Error(),
      })
    }
  })

  server.WithPreflightCheck(r, "/glossary", func(path string) {
    r.POST(path, func(c *gin.Context) {
      server.ConfigureCORS(c)
      var word Word
      if err := c.ShouldBindJSON(&word); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
      } else {
        if word.Id == uuid.Nil {
          if insert, err := Insert(&word); err == nil {
            c.JSON(http.StatusCreated, insert)
          } else {
            c.JSON(http.StatusInternalServerError, gin.H{"message": err})
          }
        } else {
          if existingWord, ok := FindById(word.Id); !ok {
            c.JSON(http.StatusConflict, gin.H{"id": existingWord.Id})
          } else {
            if insert, err := Insert(&word); err == nil {
              c.JSON(http.StatusCreated, insert)
            } else {
              c.JSON(http.StatusBadRequest, gin.H{"message": err})
            }
          }
        }
      }
    })
  })

  return r
}
