package server

import (
  "github.com/gin-gonic/gin"
  "net/http"
)

func ServerFactory() *gin.Engine {
  return gin.Default()
}

type PaginationOptions struct {
  Offset int `json:"offset"`
  Limit int `json:"limit" default:"100"`
}


func ExtractSearchQueryParameter(c *gin.Context) string {
  return c.DefaultQuery("q", "")
}

func ConfigureCORS(c *gin.Context) {
  c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
  c.Writer.Header().Set("Access-Control-Allow-Methods", "OPTIONS, POST")
  c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type")
}

func WithPreflightCheck(r *gin.Engine, endpoint string, callback func(string)) {
  r.OPTIONS(endpoint, func(c *gin.Context) {
    ConfigureCORS(c)
    c.JSON(http.StatusOK, gin.H{})
  })
  callback(endpoint)
}
