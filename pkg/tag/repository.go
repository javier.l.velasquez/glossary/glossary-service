package tag

import (
  "glossary-service/pkg/database"
  "encoding/json"
  "github.com/google/uuid"
  "github.com/jackc/pgx/v4"
  _ "github.com/jackc/pgx/v4/pgxpool"
  "log"
)

type Tag struct {
  Id   uuid.UUID `json:"id" default:"00000000-0000-0000-0000-000000000000" db:"tag_id"`
  Name string    `json:"name"`
}

func JsonToTag(b []byte) *Tag {
  data := Tag{}
  err := json.Unmarshal(b, &data)
  if err != nil {
    println(string(b))
    panic(err)
  } else {
    return &data
  }
}

func (c Tag) AsJson() []byte {
  marshal, err := json.Marshal(c)
  if err != nil {
    panic(err)
  }
  return marshal
}

func (c Tag) JsonToTag(b []byte) *Tag {
  data := Tag{}
  err := json.Unmarshal(b, &data)
  if err != nil {
    // TODO update log to include body as json
    log.Print(err)
    return &Tag{}
  } else {
    return &data
  }
}

func Insert(tag *Tag) (*Tag, error) {
  var id uuid.UUID
  err := database.ExecuteSqlInTransaction(func(row pgx.Row) error {
    return row.Scan(&id)
  }, "insert into tags (name, searchable) values ($1, to_tsvector($2)) returning id", tag.Name, tag.Name)

  if err != nil {
    return nil, err
  }

  tag.Id = id
  return tag, nil
}

func Truncate() error {
  if err := database.ExecuteTransactionAndIgnore("truncate glossary_tags CASCADE;"); err != nil {
    return err
  }
  if err := database.ExecuteTransactionAndIgnore("truncate glossary CASCADE;"); err != nil {
    return err
  }
  if err := database.ExecuteTransactionAndIgnore("truncate tags CASCADE;"); err != nil {
    return err
  }
  return nil
}

func FindByName(searchName string) (*Tag, bool) {
  var name string
  var id uuid.UUID
  err := database.FindFirst(func(row pgx.Row) error {
    return row.Scan(&id, &name)
  }, "select id, name from tags WHERE searchable @@ plainto_tsquery($1)", searchName)
  if err == nil {
    return &Tag{Id: id, Name: name}, true
  } else {
    return &Tag{}, false
  }

}

func FindById(id uuid.UUID) (*Tag, bool) {
  var name string
  err := database.FindFirst(func(row pgx.Row) error {
    return row.Scan(&name)
  }, "select name from tags where id = $1", id)
  if err == nil {
    return &Tag{Id: id, Name: name}, true
  } else {
    return &Tag{}, false
  }
}
