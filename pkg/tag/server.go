package tag

import (
  "github.com/gin-gonic/gin"
  "github.com/google/uuid"
  "glossary-service/pkg/server"
  "net/http"
)

func WithRoutes(r *gin.Engine) *gin.Engine {
  r.GET("/tags", func(c *gin.Context) {
    c.JSON(200, gin.H{
      "tags": []interface{}{},
    })
  })

  server.WithPreflightCheck(r, "/tag", func(path string) {
    r.GET("/tag", func(c *gin.Context) {
      server.ConfigureCORS(c)
      name := server.ExtractSearchQueryParameter(c)
      if tag, ok := FindByName(name); ok {
        c.JSON(http.StatusOK, tag)
      } else {
        c.JSON(http.StatusNotFound, gin.H{})
      }
    })
  })

  server.WithPreflightCheck(r, "/tags", func(path string) {
    r.POST("/tags", func(c *gin.Context) {
      server.ConfigureCORS(c)

      var tag Tag
      if err := c.ShouldBindJSON(&tag); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
      } else {
        if tag.Id == uuid.Nil {
          if insert, err := Insert(&tag); err == nil {
            c.JSON(http.StatusCreated, insert)
          } else {
            c.JSON(http.StatusInternalServerError, gin.H{"message": err})
          }
        } else {
          if existingtag, ok := FindById(tag.Id); !ok {
            c.JSON(http.StatusConflict, gin.H{"id": existingtag.Id})
          } else {
            if insert, err := Insert(&tag); err == nil {
              c.JSON(http.StatusCreated, insert)
            } else {
              c.JSON(http.StatusBadRequest, gin.H{"message": err})
            }
          }
        }
      }
    })
  })

  return r
}
